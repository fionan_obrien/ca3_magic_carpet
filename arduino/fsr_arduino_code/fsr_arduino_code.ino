#include "TimerOne.h"
#define BUFFER_WIDTH 10
#define SENSOR_COUNT 4


//for Transmission
volatile int fsrReadings[SENSOR_COUNT] = {0, 0, 0, 0};

// UP RIGHT LEFT DOWN
volatile int sensors[SENSOR_COUNT][BUFFER_WIDTH];

//for loop counters
int i, j, k, pointer;

int sensor_value;

void setup() {
  pointer = 0;

  //initialize all blank;
  for (i = 0; i < SENSOR_COUNT; i++ ) {
    for ( j = 0; j < BUFFER_WIDTH; j++ ) {
      sensors[i][j] = 0;
    }
  }

  //we will send FSR readings 60 times a second or every 16666 microseconds
  Timer1.initialize(16666);
  Timer1.attachInterrupt(printReadings);
  
  //increase baud rate for to keep up with our readings
  Serial.begin(115200);
}

void loop() {
  getReadings();

  //for each sensor
  for ( i = 0; i < SENSOR_COUNT; i++ ) {
    sensor_value = 0;
    for (j = 0; j < BUFFER_WIDTH; j++ ) {
      sensor_value += sensors[i][j];
    }
    //averaged over all buffer values;
    sensor_value /= BUFFER_WIDTH;
    fsrReadings[i] = sensor_value;
  }
}

//ok min spec
void getReadings() {
  for ( k = 0; k < SENSOR_COUNT; k++) {
    
    sensors[k][pointer] = analogRead(k);
    
    //required to limit interference
    delay(2);

  }
  pointer = (pointer + 1) % BUFFER_WIDTH;

}
void printReadings(void) {

  String up, right, down, left;
  noInterrupts();
  up = String(fsrReadings[0]);
  right = String(fsrReadings[1]);
  left = String(fsrReadings[2]);
  down = String(fsrReadings[3]);
  interrupts();

  String reading = "" + up + " " + right + " " + left + " " + down;
  Serial.println(reading);


}


