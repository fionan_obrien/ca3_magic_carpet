
/*
 * This program sends a serial array of 4 integers 
 * activating only 2 at a time 
 * cycled through 0- 2^10 
 * for creation of sample data for Magic_Carpet game
 */

int fsrReadings[4] = {0,0,0,0};
int POSSIBLE[11]={0,2,4,8,16,32,64,128,256,512,1024};


int count=0;

int UP = 0;
int DOWN = 0;
int LEFT = 0;
int RIGHT  = 0;

int modifier =1;

void setup() {
 
  Serial.begin(9600);
}

void loop() {

  getReadings();
  printReadings();
  
}

void printReadings(){
  int i =0;
  String reading = "";
  boolean read= false;
  while(i<=3){
    int value = fsrReadings[i];
    reading += " " + String(value);
    i++;
    if (value>0){read = true;}
    }
    if(read){Serial.println(reading);}
   
  }

int getNext(int val){
  
  return (val + modifier)%11;
  }

void getReadings(){

    count++;
    if (count < 100){
      DOWN =0;
      LEFT= 0;
    if(count % 5==0) UP = getNext(UP);
    if(count % 10==0) RIGHT = getNext(RIGHT);
   
    
    }else{
   
      UP =0;
      RIGHT = 0;

    if(count % 5==0) DOWN = getNext(DOWN);
    if(count % 10==0) LEFT = getNext(LEFT);
      
      }
      if(count==200){count =0;}


      fsrReadings[0] =POSSIBLE[ UP];
      fsrReadings[1] = POSSIBLE[RIGHT];
      fsrReadings[2] = POSSIBLE[DOWN];
      fsrReadings[3] = POSSIBLE[LEFT];
      delay(500);
   
  
}
