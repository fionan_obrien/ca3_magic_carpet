﻿Magic Carpet a game to demonstrate Movement control from the Arduino board.

Submitted for Ca3 Medical device software
for the attention of Peader Grant.

Why?
Reduced mobility and Dexterity users need alternative methods of Human Computer Interaction to the more traditional mouse and keyboard.
The problem I'm looking to solve is the development of an input device that will move a character in a 3d world for those users where dexterity in their fingers is limited; and a joystick is no longer suitable.
I propose that input can be taken via a pressure sensitive “balance board” that a User may place their foot and move to affect their player character in a video game.
As the user is likely to have less dexterity in their feet than an equivalent joystick thumb press; I intend to demonstrate smoothed signal output from the custom-made balance board via Arduino over USB serial interface into the Game Engine Unity to move and originate the player character/camera.

Who?
Fionán O'Brien.


Build instructions.
-required Unity development studio 5 avaiable from https://unity3d.com/get-unity/download
-This project is Windows only
-run Build.bat to generate documentation
-Note Automated GameBuild requires a user log in and password and has been disabled on the current build
-Other builds may work but have not been tested
-Arduino IDE https://www.arduino.cc/