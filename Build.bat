
cd %cd%\docs
bibtex ca3_documentation
pdflatex ca3_documentation.tex 

cd ..

GOTO SKIP_BUILD
set UNITY ="C:\Program files\Unity\Editor\Unity.exe"
set PROJECT_PATH="%cd%"
set BATCH=-batchmode
set QUIT=-quit
set PROJECT=-projectPath=%PROJECT_PATH%
set WIN_PATH="%cd%\mc.exe"
set WIN=-buildWindows64Player=%WIN_PATH%

"C:\Program files\Unity\Editor\Unity.exe" %BATCH% %QUIT% %PROJECT% %WIN%

:SKIP_BUILD
start %cd%\docs\ca3_documentation.pdf

pause