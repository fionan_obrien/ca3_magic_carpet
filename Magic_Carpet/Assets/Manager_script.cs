﻿using UnityEngine;
using System.Collections;

public class Manager_script : MonoBehaviour {

    public GameObject menu;
    public AudioSource music;
    public CarpetPilot player;

    private int game_sensitivity = 384;

    public void gameStart() {


        CarpetPilot.GAME_STARTED = true;
        CarpetPilot.sensitivity = game_sensitivity;
        music.Play();
        player.reset_position();



    }


    void Update()
    {
        if (Input.GetKeyDown("escape")) {
            menu.SetActive(!menu.activeSelf);
        }
            

    }

    public void exit() {

        Application.Quit();
    }
    public void set_sensitivity(int choice) {

        Debug.Log("YAY!");

        switch (choice) {

            case 0:
                game_sensitivity = 256;
                break;
            case 1 :
                game_sensitivity = 384;
                break;
            case 2:
                game_sensitivity = 512;
                break;


        }


    }
}
