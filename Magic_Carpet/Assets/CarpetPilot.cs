﻿using UnityEngine;
using System.Collections;
using System;

public class CarpetPilot : MonoBehaviour
{
    static public bool GAME_STARTED = false;
    static public int sensitivity = 384;

    public float speed = 30.0f;
    public float max_speed = 75.0f;
    public float min_speed = 10.0f;
    public float horizontal_modifier = 1f;
    Vector3 Start_location;
    Quaternion Start_rotation;

    string[] values;
    GameObject[] info_boxes;
    int[] last_values = { 0, 0, 0, 0 };
    bool connected = false;
    int indicator_Tolerance = 100;

    // Use this for initialization
    void Start()
    {
        Debug.Log("carpet pilot script added to:" + gameObject.name);
        Start_location = transform.position;
        Start_rotation = transform.rotation;

        //setup info boxes
        info_boxes = new GameObject[4];
        info_boxes[0] = GameObject.Find("front");
        info_boxes[1] = GameObject.Find("right");
        info_boxes[2] = GameObject.Find("left");
        info_boxes[3] = GameObject.Find("back");



        Renderer rend1 = info_boxes[0].GetComponent<Renderer>();
        Renderer rend2 = info_boxes[1].GetComponent<Renderer>();
        Renderer rend3 = info_boxes[2].GetComponent<Renderer>();
        Renderer rend4 = info_boxes[3].GetComponent<Renderer>();
        rend1.material.shader = Shader.Find("Specular");
        rend2.material.shader = Shader.Find("Specular");
        rend3.material.shader = Shader.Find("Specular");
        rend4.material.shader = Shader.Find("Specular");
    }

    // Update is called once per frame
    void Update()
    {
        if (GAME_STARTED)
        {
            //Camera Update
            Vector3 moveCamTo = transform.position - transform.forward * 10.0f + Vector3.up * 5.0f;
            float bias = 0.96f;
            Camera.main.transform.position = Camera.main.transform.position * bias + moveCamTo * (1.0f - bias);
            Camera.main.transform.LookAt(transform.position + transform.forward * 20.0f);

            //Movement update
            transform.position += transform.forward * Time.deltaTime * speed;

            speed -= transform.forward.y * Time.deltaTime * 50.0f;
            //do not exceed max and min speeds
            if (speed < min_speed) { speed = min_speed; }
            if (speed > max_speed) { speed = max_speed; }



            //Keyboard input 
            //Rotate around its own axis 1 unit Vertical based on axis pressed and negative 1.5 Horizontal per frame

            transform.Rotate(Input.GetAxis("Vertical"), 0.0f, -Input.GetAxis("Horizontal") * horizontal_modifier);

            //get Rotation from serial values recieved.
            onUpdateRotate();


            //check land colider
            float terrainHeightWhereWeAre = Terrain.activeTerrain.SampleHeight(transform.position);

            if (terrainHeightWhereWeAre > transform.position.y)
            {
                transform.position = new Vector3(transform.position.x,
                                                terrainHeightWhereWeAre,
                                                transform.position.z);

            }

            if(transform.position.x > 1010 || transform.position.z >1010 ||
                transform.position.x < -0 || transform.position.z < -0
                ) {
                this.reset_position();
                    
                    }


        }
    }

    
    

    void onUpdateRotate() {
        if (connected)
            //only update when serial connection is made
        {
            if (values.Length == 4)
            {
                //up right left down

                // Rotate the carpet
                 transform.Rotate(Vector3.right * (last_values[0] / sensitivity)); //up arrow
                 transform.Rotate(Vector3.back *  (last_values[1] / sensitivity * horizontal_modifier)); // 
                 transform.Rotate(Vector3.left * (last_values[3] / sensitivity)); //right // left
                 transform.Rotate(Vector3.forward  * (last_values[2] / sensitivity * horizontal_modifier));//up
                
              
                updateInfoTabs();
            }
            
        }
    }

    void updateInfoTabs() {
        //updates the color of the 4 info boxes on the Magic Carpet model

        string mat_type = "_Color";

        Color ON = Color.green;
      
        Color neutral = Color.white;

        for (int i = 0; i < 4; i++) {

           if( last_values[i] > indicator_Tolerance)
            {
                info_boxes[i].GetComponent<Renderer>().material.SetColor(mat_type, ON);


            }else {
                info_boxes[i].GetComponent<Renderer>().material.SetColor(mat_type, neutral);
            }

        }

    }

    void OnSerialLine(string line)
    {
        /*
         * 
         * This function  parses 4 integer values from the serial port
         * 
         */

        char[] delimiters = new char[] { ' ', '\n', '\r' };
        string[] new_values = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);



        if (new_values.Length == 4)
        {
            values = new_values;
            int pos = 0;

            try
            {
                foreach (string s in values)
                {

                    last_values[pos++] = System.Int32.Parse(s);
                }
                connected = true;

            }
            catch (FormatException)
            {
                connected = false;
            }

        }

    }


    public void reset_position() {

        transform.position = Start_location;
        transform.rotation = Start_rotation;
    }

}