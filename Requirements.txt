Requirements:

	-Unity development studio 5 avaiable from https://unity3d.com/get-unity/download
	-Microsoft Windows version 10.0
	-Arduino IDE or AVR complier
	-Latex for documentaion creation

Build:
	Documentation
	-to build documentation in windows run Build.bat

	Magic Carpet Game
	(Note Headless Automated builds have been disabled without a User and Password for unity)
	-To build Game load up Unity.exe
	-load project folder
	-select file build & run

	Arduino Microcontroller
	-Run AVR arduino\fsr_arduino_code\fsr_arduino_code.ino
	
	Balance board
	-attach the pin block (black side) to pins A0-A4
	-attach a cable from last 2 pins (white side) to 5v ground respectively


Software for inspection
	Arduino
	- \arduino\fsr_arduino_code\fsr_arduino_code.ino
	- \arduino\fsr_arduino_code\Test_program\Test_program.ino

	Magic Carpet game
	-\Magic_Carpet\Assets\CarpetPilot.cs
	-\Magic_Carpet\Assets\Manager_script.cs

Third Party liabries

	Serial (C#)
	-\Magic_Carpet\Assets\Serial\

	TimerOne
	-\arduino\fsr_arduino_code\TimerOne.cpp
	-\arduino\fsr_arduino_code\TimerOne.h
	-\arduino\fsr_arduino_code\config\known_16bit_timers.h